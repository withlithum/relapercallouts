﻿// Copyright (C) RelaperCrystal 2021.
// Licensed under GNU GPL v3.

using Rage;

namespace RelaperCallouts.Util.Entities
{
    internal struct DisturbanceSpawnInfo
    {
        internal Vector3 GuardSpawn;
        internal float GuardHeading;
        internal Vector3 SuspectSpawn;
        internal float SuspectHeading;
        internal string Name;
    }
}
