﻿// Copyright (C) RelaperCrystal 2021.
// Licensed under GNU GPL v3.

// As a special notice, you are permitted to copy the file freely without 
// following "copyleft" restrictions
// of the license, with or without modification, starting from line 17, with proper credits.

using System;
using System.Collections.Generic;
using Rage;
using UltimateBackup.API;

namespace RelaperCallouts.Extern
{
    internal static class UltimateBackupFunctions
    {
        internal static void RequestLocalUnitCodeThree()
        {
            Functions.callCode3Backup();
        }

        internal static Ped GetLocalUnitPed(Vector3 location, float heading)
        {
            return Functions.getLocalPatrolPed(location, heading);
        }

        internal static Tuple<Vehicle, List<Ped>> GetLocalUnit(Vector3 location, int numPeds)
        {
            return Functions.getLocalPatrolUnit(location, numPeds);
        }
    }
}
