﻿// Copyright (C) RelaperCrystal 2021.
// Licensed under GNU GPL v3.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Rage;
using RelaperCallouts.Util.Entities;

namespace RelaperCallouts.Callouts.Disturbance
{
    internal static class DisturbanceUtil
    {
        internal static readonly DisturbanceSpawnInfo[] spawnInfos = new DisturbanceSpawnInfo[]
        {
            new DisturbanceSpawnInfo()
            {
                Name = "Diamond Casino Resort",
                GuardSpawn = new Vector3(936.7716f, 46.40448f, 81.09574f),
                GuardHeading = 137.2896f,
                SuspectSpawn = new Vector3(928.9203f, 44.693f, 81.09574f),
                SuspectHeading = 287.5472f
            },
            new DisturbanceSpawnInfo()
            {
                Name = "Construction Site",
                GuardSpawn = new Vector3(6.189239f, -410.1216f, 39.36133f),
                GuardHeading = 44.44086f,
                SuspectSpawn = new Vector3(0.3701473f, -404.5615f, 39.38879f),
                SuspectHeading = 235.0367f
            }
        };

        internal static bool TryGetSpawnInfo(out DisturbanceSpawnInfo info)
        {
            foreach (var spawn in spawnInfos)
            {
                Game.LogTrivial("Rel.C: Checking " + spawn.Name + " for Disturbance call");
                if (spawn.GuardSpawn.DistanceTo(Game.LocalPlayer.Character) > 850f)
                {
                    Game.LogTrivial("Rel.C: " + spawn.Name + " is too far away");
                }
                else
                {
                    Game.LogTrivial("Rel.C: Selected " + spawn.Name + " for Disturbance call");
                    info = spawn;
                    return true;
                }
            }

            Game.LogTrivial("Rel.C: Failed to find spawn location for Disturbance");
            info = default;
            return false;
        }
    }
}
