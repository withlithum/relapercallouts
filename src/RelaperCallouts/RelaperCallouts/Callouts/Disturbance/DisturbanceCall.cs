﻿// Copyright (C) RelaperCrystal 2021.
// Licensed under GNU GPL v3.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LSPD_First_Response.Mod.Callouts;
using Rage;
using RelaperCallouts.Callouts.Framework;
using RelaperCallouts.Util.Entities;
using RelaperCommons;
using RelaperCommons.Entities;

namespace RelaperCallouts.Callouts.Disturbance
{
    [CalloutInfo("Disturbance RC", CalloutProbability.Medium)]
    [ExperimentalCallout(DebugModeOnly = true)]
    public class DisturbanceCall : CalloutBase
    {
        private static readonly string[] guardDialogues = new string[]
        {
            "~b~Guard~w~: Hello officer.",
            "~b~Guard~w~: This person is forbidden to enter this premise.",
            "~b~Guard~w~: So this person has to leave."
        };

        private static readonly string[] suspectFleeDialogues = new string[]
        {
            "~r~Suspect~w~: I'm leaving, ya prick!",
            "~r~Suspect~w~: Screw you!",
            "~r~Suspect~w~: Get the hell away from me!",
            "~r~Suspect~w~: No!"
        };

        private static readonly string[] suspectAgreeDialogues = new string[]
        {
            "~r~Suspect~w~: Screw this. I'm going.",
            "~r~Suspect~w~: I don't want to mess with this.",
            "~r~Suspect~w~: I thought you wouldn't call the cops.",
            "~r~Suspect~w~: Ok, ok. I'm going."
        };

        private static readonly string[] suspectFightDialogues = new string[]
        {
            "~r~Suspect~w~: You are dead you son of b**ch!",
            "~r~Suspect~w~: Get the hell away from me!",
            "~r~Suspect~w~: Eat this you sh*thead!",
            "~r~Suspect~w~: MOTHERF**KER!"
        };

        private static readonly string[] suspectShootDialogues = new string[]
        {
            "~r~Suspect~w~: Surprise motherf**ker!",
            "~r~Suspect~w~: I have a gun you f**k!",
            "~r~Suspect~w~: You won't except THIS!"
        };

        private DisturbanceSpawnInfo spawnInfo;
        private Ped guard;
        private Ped suspect;
        private bool arrived;
        private int situation;

        protected override string Name => "Disturbance";

        protected override string ScannerCrimeName => "DISTURBANCE";
        protected override bool HasNormalCrimeAudio => true;
        protected override bool HasReportCrimeAudio => true;

        public override bool OnBeforeCalloutDisplayed()
        {
            // Failed to find a spawn point will cause this call to be terminated
            if (!DisturbanceUtil.TryGetSpawnInfo(out spawnInfo))
            {
                terminated = false;
                return false;
            }

            ResponseType = CalloutResponseType.Code2;
            ReportedByUnits = false;
            CalloutPosition = World.GetNextPositionOnStreet(spawnInfo.GuardSpawn);

            return base.OnBeforeCalloutDisplayed();
        }

        public override bool OnCalloutAccepted()
        {
            guard = new Ped("S_M_M_SECURITY_01", spawnInfo.GuardSpawn, spawnInfo.GuardHeading)
            {
                IsPersistent = true,
                BlockPermanentEvents = true
            };
            suspect = new Ped(spawnInfo.SuspectSpawn, spawnInfo.SuspectHeading) 
            {
                IsPersistent = true,
                BlockPermanentEvents = true
            };

            situation = MathHelper.GetRandomInteger(0, 4);

            Blip = new Blip(CalloutPosition, 85f)
            {
                IsRouteEnabled = true,
                Alpha = 0.45f
            };

            Blip.SetColor(BlipColor.Yellow);

            return base.OnCalloutAccepted();
        }

        public override void Process()
        {
            base.Process();

            if (!arrived && Game.LocalPlayer.Character.Position.DistanceTo2D(spawnInfo.GuardSpawn) < 83f && (guard.IsOnScreen || suspect.IsOnScreen))
            {
                arrived = true;

            }
        }
    }
}
