﻿// Copyright (C) RelaperCrystal 2021.
// Licensed under GNU GPL v3.

using System;

namespace RelaperCallouts.Callouts.Framework
{
    /// <summary>
    /// Marks out an experiment call-out which blocks some actions.
    /// </summary>
    /// <seealso cref="System.Attribute" />
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
    internal sealed class ExperimentalCalloutAttribute : Attribute
    {
        /// <summary>
        /// Gets or sets a value indicating whether this call-out will only register in debug binaries.
        /// </summary>
        /// <value>
        ///   <c>true</c> if register only when debug mode; otherwise, <c>false</c>.
        /// </value>
        public bool DebugModeOnly { get; set; }
    }
}