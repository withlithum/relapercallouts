﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LSPD_First_Response.Mod.API;
using LSPD_First_Response.Mod.Callouts;
using Rage;

namespace RelaperCallouts.Callouts.Framework
{
    internal static class CalloutRegistry
    {
        internal static void Register(Type type)
        {
            foreach (var obj in type.GetCustomAttributes(false))
            {
                GameFiber.Yield();
                if (obj is ExperimentalCalloutAttribute exper)
                {
                    Game.DisplayNotification($"~r~~b~REL.C WARNING~n~~s~Call {type.Name} is currently EXPERIMENTAL. It might break at ~b~any minute~s~. Use ~r~at your own risk~w~.");
                    if (exper.DebugModeOnly)
                    {
                        Game.LogTrivial("Rel.C: Ignored experiment call " + type.Name);
                        return;
                    }
                }
            }

            Game.LogTrivial("Rel.C: Registering call-out " + type.Name);
            Functions.RegisterCallout(type);
        }
    }
}
