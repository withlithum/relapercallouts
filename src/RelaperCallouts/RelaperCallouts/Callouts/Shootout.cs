﻿// Copyright (C) RelaperCrystal 2021.
// Licensed under GNU GPL v3.

using System;
using System.Collections.Generic;
using System.Linq;
using LSPD_First_Response;
using LSPD_First_Response.Mod.API;
using LSPD_First_Response.Mod.Callouts;
using Rage;
using RelaperCallouts.Callouts.Framework;
using RelaperCallouts.Extern;
using RelaperCallouts.Util;
using RelaperCommons;
using RelaperCommons.Entities;

namespace RelaperCallouts.Callouts
{
    [CalloutInfo("Shootout", CalloutProbability.Medium)]
    public class Shootout : CalloutBase
    {
        private readonly List<Ped> peds = new List<Ped>();
        private readonly List<Ped> dealtPeds = new List<Ped>();
        private readonly Dictionary<Ped, Blip> pedBlips = new Dictionary<Ped, Blip>();
        private static RelationshipGroup _shooterGroup = new RelationshipGroup("RCSHOTGRP");
        private bool discovered;

        protected override string Name => "Shots fired";

        protected override string ScannerCrimeName => "SHOOTING";

        protected override bool HasNormalCrimeAudio => true;
        protected override bool HasReportCrimeAudio => false;

        // We need this to be called for Initialize
        // to start shooter group...
        internal static void InitShooterGroup()
        {
            _shooterGroup.SetRelationshipWith(_shooterGroup, Relationship.Hate);
            _shooterGroup.SetRelationshipWith(RelationshipGroup.Cop, Relationship.Hate);
            _shooterGroup.SetRelationshipWith(RelationshipGroup.Player, Relationship.Hate);
        }

        public override bool OnBeforeCalloutDisplayed()
        {
            ResponseType = CalloutResponseType.Code3;
            ReportedByUnits = MathHelper.GetRandomInteger(11) != 5;
            SpawnPoint = SpawnUtil.GenerateSpawnPointAroundPlayer(350f, 750f);
            ReportedByUnits = false;

            this.AddMinimumDistanceCheck(150f, SpawnPoint);
            this.AddMaximumDistanceCheck(550f, SpawnPoint);

            return base.OnBeforeCalloutDisplayed();
        }

        public override bool OnCalloutAccepted()
        {
            var spawnAmount = MathHelper.GetRandomInteger(4, 15);
            for (var i = 0; i < spawnAmount; i++)
            {
                GameFiber.Yield();
                Ped ped;

                if (ExternManager.ResponseReplaceInstalled)
                {
                    ped = ResponseReplaceFunctions.CreatePed(SpawnPoint.Around(3.3f, 12.2f));
                    ped.IsPersistent = true;
                }
                else
                {
                    ped = new Ped(SpawnPoint.Around(3.3f, 12.2f))
                    {
                        IsPersistent = true
                    };
                }

                ped.Inventory.GiveNewWeapon(WeaponHash.Pistol, 200, true);
                ped.RelationshipGroup = _shooterGroup;
                var blip = ped.AttachBlip();
                blip.Scale = 0.50f;
                blip.Sprite = BlipSprite.Enemy;
                blip.SetColor(BlipColor.Red);
                blip.IsFriendly = false;
                pedBlips.Add(ped, blip);

                peds.Add(ped);
            }

            Blip = new Blip(World.GetNextPositionOnStreet(SpawnPoint.Around(10f)), 65f);
            Blip.SetColor(BlipColor.Yellow);
            Blip.SetRouteColor(BlipColor.Yellow);
            Blip.IsRouteEnabled = true;

            Game.DisplaySubtitle("Get to the ~y~crime scene~w~.");

            return base.OnCalloutAccepted();
        }

        public override void Process()
        {
            if (!discovered && Game.LocalPlayer.Character.Position.DistanceTo2D(CalloutPosition) < 55f)
            {
                discovered = true;
                Game.DisplaySubtitle("Arrest or take out the ~r~suspect~w~s.");
                Blip.IsFriendly = false;
                Blip.Flash(500, -1);
                Blip.Alpha = 0.45f;
                Blip.IsRouteEnabled = false;
                if (ExternManager.UltimateBackupInstalled)
                {
                    UltimateBackupFunctions.RequestLocalUnitCodeThree();
                }
                else
                {
                    Functions.RequestBackup(SpawnPoint, EBackupResponseType.Code3, EBackupUnitType.LocalUnit);
                }
            }

            for (int i = 0; i < peds.Count; i++)
            {
                GameFiber.Yield();
                var ped = peds[i];
                if (!ped || !ped.IsAlive || Functions.IsPedArrested(ped))
                {
                    peds.RemoveAt(i);
                    if (ped)
                    {
                        pedBlips[ped].Delete();

                        dealtPeds.Add(ped);
                    }
                }
            }

            if (peds.Count == 0)
            {
                EndSuccess();
            }

            base.Process();
        }

        public override void End()
        {
            base.End();

            foreach (var ped in peds)
            {
                if (ped && !Functions.IsPedArrested(ped))
                {
                    if (ped.IsAlive && ExternManager.ResponseReplaceInstalled)
                    {
                        try
                        {
                            ResponseReplaceFunctions.RegisterPed(ped);
                        }
#pragma warning disable CA1031 // Do not catch general exception types
                        catch (Exception ex)
                        {
                            Game.LogTrivial(ex.ToString());
                        }
#pragma warning restore CA1031 // Do not catch general exception types
                    }
                    ped.Dismiss();
                }
            }

            foreach (var ped in dealtPeds.Where(ped => ped && !Functions.IsPedArrested(ped)))
            {
                ped.Dismiss();
            }

            foreach (var blip in pedBlips.Where(blip => blip.Value))
            {
                blip.Value.Delete();
            }
        }
    }
}
